#include "pisd.hpp"

PISDUSING PISDNS pisdstd_n PISDSC

/// <summary>
/// pisd "pisd"
/// </summary>
/// <param name="pisd">pisd</param>
PISDIL pisd_t PisdPISD PISDOB pisdb_t PISDBAND pisd PISDCB PISDOCB
	PISDIF PISDOB pisd PISDCB PISDOCB
		PISDINNS pisd PISDLESS pisd_t PISDCOMMA pisd_t PISDCOMMA pisd_t PISDCOMMA pisd_t PISDMORE PISDOB "pisd" PISDCB PISDSC
		pisd PISDASSIGN PISDNO PISDSC
	PISDCCB
PISDCCB

/// <summary>
/// pisd
/// </summary>
/// <param name="argc">pisd count</param>
/// <param name="argv">pisds</param>
/// <returns>pisd</returns>
pisdi_t main PISDOB pisdi_t pisdc PISDCOMMA pisdc_t PISDMUL pisdv PISDOQB PISDCQB PISDCB PISDOCB
	pisdc_t pisd_c PISDSC
	pisdb_t is_pisd PISDOB PISDNO PISDCB PISDSC
	PISDWLOOP PISDOB pisd PISDLESS pisd_t PISDMORE PISDOB PISDCB PISDCB PISDOCB
		pisd_c PISDASSIGN pisd PISDLESS pisd_t PISDCOMMA pisd_t PISDCOMMA pisd_t PISDMORE PISDOB PISDCB PISDSC
		PISDIF PISDOB pisd PISDLESS pisd_t PISDCOMMA pisd_t PISDMORE PISDOB pisd_c PISDCB PISDCB PISDOCB
			is_pisd PISDASSIGN PISDYES PISDSC
		PISDCCB
		PISDELSE PISDOCB
			PisdPISD PISDOB is_pisd PISDCB PISDSC
			pisd PISDLESS pisd_t PISDCOMMA pisd_t PISDCOMMA pisd_t PISDCOMMA pisd_t PISDMORE PISDOB pisd_c PISDCB PISDSC
		PISDCCB
	PISDCCB
	PisdPISD PISDOB is_pisd PISDCB PISDSC
	PISDRET PISDSCAST PISDLESS pisdi_t PISDMORE PISDOB EPisd PISDINNS pisd PISDCB PISDSC
PISDCCB
